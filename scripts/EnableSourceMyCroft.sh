#!/bin/bash

. /opt/qbo/mycroft-core/.venv/bin/activate

python /opt/qbo/assistants/QboMyCroft.py "$1"